﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Transform player;

    private float smoothSpeed = 0.125f;

    [SerializeField] private Vector3 offset; //hur långt bort kameran ska vara från spelaren

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
    private void Update()
    {
        if(player == null)
        {
            GameObject playerGameObject = GameObject.FindGameObjectWithTag("Player");
            if(playerGameObject != null)
            {
                player = playerGameObject.transform;
            }
        }
    }
    private void FixedUpdate()
    {
        if(player != null)
        {
            Vector3 desiredPosition = player.position + offset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = smoothedPosition;

            //transform.LookAt(player);
        }
    }
}
