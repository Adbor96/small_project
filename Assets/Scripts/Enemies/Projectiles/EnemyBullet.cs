﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    [SerializeField] private float speed;
    private int damage;
    private Vector3 playerPos;
    private Vector3 direction;
    private Rigidbody2D myRb;
    // Start is called before the first frame update
    void Start()
    {
        myRb = GetComponent<Rigidbody2D>();
        Vector3 directionX = (playerPos - transform.position);
        direction = new Vector2(directionX.x, 0).normalized;
        if(playerPos.x <= transform.position.x)
        {
            transform.localScale = new Vector2(1, -1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Fire();
    }
    public void Setup(int _damage, Vector2 _playerPos)
    {
        damage = _damage;
        playerPos = _playerPos;
    }
    private void Fire()
    {
        myRb.velocity = direction.normalized * speed;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Player player = collision.gameObject.GetComponent<Player>();
        if(player != null)
        {
            Debug.Log("bullet damage" + damage);
            player.TakeDamage(damage);
            Destroy(gameObject);
        }
        Destroy(gameObject);
    }
}
