﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    protected enum States
    {
        idle, moving, shooting, melee, dead
    }

    [SerializeField] private EnemyObject enemyData;
    protected int health;
    protected int ammo;
    protected int damage;
    protected float speed;
    protected float radius;
    protected float playerDistance;
    protected float meleeDistance;
    protected EnemyBullet bulletPrefab;
    protected EnemyFire firePrefab;

    protected Vector3 startingPosition;

    //hur lång tid det ska vara mellan två skott från fienden
    protected float fireRate;
    protected float nextAttackTime;

    protected float meleeRate;
    protected float nextMeleeTime;

    protected GameObject player;

    protected Transform bulletExit;

    protected States currentState;

    protected Rigidbody2D myRb;

    protected EnemyRespawn respawnManager;
    // Start is called before the first frame update
    void Start()
    {
        Setup();
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public virtual void Setup()
    {
        myRb = GetComponent<Rigidbody2D>();
        health = enemyData.health;
        ammo = enemyData.ammo;
        speed = enemyData.speed;
        damage = enemyData.damage;
        fireRate = enemyData.fireRate;
        playerDistance = enemyData.playerDistance;
        bulletPrefab = enemyData.bulletPrefab;
        firePrefab = enemyData.firePrefab;
        bulletExit = transform.GetChild(0).transform;
        player = GameObject.FindGameObjectWithTag("Player");
        currentState = States.idle;
        radius = enemyData.searchRadius;
        meleeDistance = enemyData.meleeRange;
        meleeRate = enemyData.meleeRate;
        respawnManager = GameObject.Find("EnemyManager").GetComponent<EnemyRespawn>();

        startingPosition = transform.position;
    }
    public virtual void Shoot()
    {

    }
    protected void Flip()
    {
        if(player.transform.position.x < transform.position.x)
        {
            transform.localScale = new Vector2(-1, 1);
        }
        else
        {
            transform.localScale = new Vector2(1, 1);
        }
    }
    public virtual void TakeDamage(int _damage)
    {

        
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, enemyData.searchRadius);
    }
    protected virtual void Melee()
    {

    }
}
