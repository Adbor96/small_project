﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemy : Enemy
{
    private Animator myAnim;
    [SerializeField] private LayerMask playerMask;
    private AudioSource source;
    [SerializeField] private float jumpForce;
    [SerializeField] private AudioClip fireAudio;
    [SerializeField] private AudioClip meleeAudio;
    [SerializeField] private AudioClip[] damagedAudio;
    [SerializeField] private AudioClip deathAudio;
    [SerializeField] private LayerMask groundMask;
    // Start is called before the first frame update
    void Start()
    {
        //myAnim.SetTrigger("Idle");
        Setup();
        Debug.Log(currentState);
        myAnim = GetComponent<Animator>();
        myAnim.SetInteger("Health", health);
        source = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            return;
        }
        else
        {

        }
        if(currentState == States.moving)
        {
            myAnim.SetBool("Running", true); //om fienden rör på sig, spela "run" animationen
        }
        else
        {
            myAnim.SetBool("Running", false);
        }
        if(player != null && currentState != States.dead)
        {
            Flip();
        }
        myAnim.SetInteger("Health", health);
        //Debug.Log(player.transform.position.x);
        switch (currentState)
        {
            case States.idle:
                //fienden gör inget när den är idle, utan söker bara efter spelaren
                Collider2D[] _colliders = Physics2D.OverlapCircleAll(transform.position, radius);
                //Debug.Log(_colliders);
                foreach (Collider2D _hitObject in _colliders)
                {
                    if(_hitObject.tag == "Player")
                    {
                        Debug.Log("Found player");
                        currentState = States.moving;
                        break;
                    }
                }
                /*if(Vector2.Distance(transform.position, player.transform.position) <= radius)
                {
                    Debug.Log("Found player");
                    currentState = States.moving;
                }*/
                break;
            case States.moving:
                Vector2 playerDir = new Vector2(player.transform.position.x - transform.position.x, 0).normalized;
                myRb.velocity = new Vector2(playerDir.x * speed, myRb.velocity.y);
                    if (Time.time >= nextAttackTime)
                    {
                        currentState = States.shooting;
                    }
                if(Vector2.Distance(transform.position, player.transform.position) <= meleeDistance)
                {
                    Debug.DrawLine(transform.position, player.transform.position);
                    currentState = States.melee;
                }
                Collider2D _ground = Physics2D.OverlapCircle(bulletExit.position, 0.8f, groundMask);
                if(_ground != null)
                {
                    myRb.AddForce(new Vector2(0, jumpForce));
                }
                else
                {
                    Debug.Log(_ground);
                }

                break;
            case States.shooting:
                myRb.velocity = Vector2.zero;
                Shoot();
                if (Vector2.Distance(transform.position, player.transform.position) <= meleeDistance)
                {
                    Debug.DrawLine(transform.position, player.transform.position);
                    currentState = States.melee;
                }
                break;
            case States.melee:
                if(Time.time >= nextMeleeTime)
                {
                    Melee();
                }
                if (Vector2.Distance(transform.position, player.transform.position) > meleeDistance)
                {
                    currentState = States.moving;
                }
                    break;
            case States.dead:
                break;
        }
    }
    public override void Shoot()
    {
        myAnim.SetTrigger("Shoot");
        source.PlayOneShot(fireAudio);
        Vector2 playerPos = player.transform.position;

            EnemyBullet bullet = Instantiate(bulletPrefab, bulletExit.position, bulletPrefab.transform.rotation);
            bullet.Setup(damage, playerPos);
            nextAttackTime = Time.time + 1f / fireRate;
        currentState = States.moving;

    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
        Gizmos.color = Color.red;

    }
    private void OnDrawGizmosSelected()
    {
        //Gizmos.DrawWireSphere(bulletExit.position, 0.9f);
    }
    public override void TakeDamage(int _damage)
    {
        health -= _damage;
        if (health <= 0)
        {
            source.PlayOneShot(deathAudio);
            respawnManager.deadEnemies.Add(1);
            respawnManager.enemyPositions.Add(startingPosition);
            if(currentState != States.dead)
            {
                currentState = States.dead;
                myAnim.SetTrigger("Death");
                GetComponent<Collider2D>().enabled = false;
                GetComponent<Rigidbody2D>().isKinematic = true;
                myRb.velocity = Vector2.zero;
                Destroy(gameObject, 2);
            }
        }
        else
        {
            source.PlayOneShot(damagedAudio[Random.Range(0, damagedAudio.Length)]);
        }
    }
    protected override void Melee()
    {
        //melee attack
        myAnim.SetTrigger("Melee");
        source.PlayOneShot(meleeAudio);
        //kolla efter spelare i närheten
        Collider2D _player = Physics2D.OverlapCircle(bulletExit.position, meleeDistance, playerMask);

        if(_player != null)
        {
            Player player = _player.gameObject.GetComponent<Player>();
            if (player != null)
            {
                Debug.Log("melee damage" + damage);
                player.TakeDamage(damage);
            }
            nextMeleeTime = Time.time + 1f / meleeRate;
        }
    }
}
