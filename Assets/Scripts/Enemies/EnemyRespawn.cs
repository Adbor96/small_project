﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRespawn : MonoBehaviour
{
    public List<Vector3> enemyPositions;
    public List<int> deadEnemies;
    [SerializeField] private GameObject enemyPrefab;
    private BossTrigger bossTrigger;
    // Start is called before the first frame update
    void Start()
    {
        bossTrigger = GameObject.FindObjectOfType<BossTrigger>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Respawn()
    {
        for (int i = 0; i < deadEnemies.Count; i++)
        {
            GameObject respawnedEnemy = Instantiate(enemyPrefab, enemyPositions[i], Quaternion.identity);
        }
        enemyPositions.Clear();
        deadEnemies.Clear();
        bossTrigger.GetComponent<BoxCollider2D>().enabled = true;
    }
}
