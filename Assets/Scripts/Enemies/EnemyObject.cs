﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemies/Enemy")]
public class EnemyObject : ScriptableObject
{
    public int health;
    public float speed;
    public int damage;
    public int ammo;
    public float fireRate;
    public float searchRadius;
    public EnemyBullet bulletPrefab;
    public EnemyFire firePrefab;
    public float playerDistance;
    public float meleeRange;
    public float meleeRate;

}
