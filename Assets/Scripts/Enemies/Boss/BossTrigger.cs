﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTrigger : MonoBehaviour
{
    [SerializeField] private Boss bossPrefab;
    private PlayerMovement player;
    private Transform spawnPoint;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindObjectOfType<PlayerMovement>();
        spawnPoint = transform.GetChild(0).transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            player = GameObject.FindObjectOfType<PlayerMovement>();
            return;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            StartCoroutine(BossSpawn());
        }
    }
    private IEnumerator BossSpawn()
    {
        GetComponent<BoxCollider2D>().enabled = false;
        player.runSpeed = 0;
        player.GetComponent<Weapon>().canShoot = false;

        yield return new WaitForSeconds(2);

        Boss _boss = Instantiate(bossPrefab, spawnPoint.position, Quaternion.identity);
        yield return new WaitForSeconds(1);
        _boss.PlayIntroSound();
        yield return new WaitForSeconds(2);
        player.runSpeed = 40;
        player.GetComponent<Weapon>().canShoot = true;
        _boss.GoIntoCombat();

    }
}
