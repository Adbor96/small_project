﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Boss : Enemy
{
    // Start is called before the first frame update
    private Animator myAnim;
    [SerializeField] private LayerMask playerMask;

    [SerializeField] private SpriteRenderer explosion;
    private AudioSource source;
    [SerializeField] private AudioClip[] damagedSounds;
    private bool canPlaySound;
    [SerializeField] private AudioClip introSound;
    [SerializeField] private AudioClip deathSound;
    [SerializeField] private AudioClip fireSound;
    [SerializeField] private AudioClip explosionSound;

    public event EventHandler onBossDefeated;
    void Start()
    {
        Setup();
        //Debug.Log(currentState);
        myAnim = GetComponent<Animator>();
        myAnim.SetInteger("Health", health);
        source = GetComponent<AudioSource>();
        canPlaySound = true;
        Debug.Log(canPlaySound);
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            return;
        }
        else
        if (currentState == States.moving)
        {
            //myAnim.SetBool("Running", true); //om fienden rör på sig, spela "run" animationen
        }
        else
        {
            //myAnim.SetBool("Running", false);
        }
        if (player != null && currentState != States.dead)
        {
            Flip();
        }
        //myAnim.SetInteger("Health", health);
        //Debug.Log(player.transform.position.x);
        switch (currentState)
        {
            case States.idle:
                //fienden gör inget när den är idle, utan söker bara efter spelaren
                /*Collider2D[] _colliders = Physics2D.OverlapCircleAll(transform.position, radius);
                //Debug.Log(_colliders);
                foreach (Collider2D _hitObject in _colliders)
                {
                    if (_hitObject.tag == "Player")
                    {
                        Debug.Log("Found player");
                        currentState = States.moving;
                        break;
                    }
                }*/
                /*if(Vector2.Distance(transform.position, player.transform.position) <= radius)
                {
                    Debug.Log("Found player");
                    currentState = States.moving;
                }*/
                break;
            case States.moving:
                Vector2 playerDir = new Vector2(player.transform.position.x - transform.position.x, 0).normalized;
                myRb.velocity = new Vector2(playerDir.x * speed, myRb.velocity.y);
                if (Time.time >= nextAttackTime)
                {
                    currentState = States.shooting;
                }

                break;
            case States.shooting:
                myRb.velocity = Vector2.zero;
                Shoot();
                if (Vector2.Distance(transform.position, player.transform.position) <= meleeDistance)
                {
                    Debug.DrawLine(transform.position, player.transform.position);
                    currentState = States.melee;
                }
                break;
            case States.melee:
                if (Time.time >= nextMeleeTime)
                {
                    Melee();
                }
                if (Vector2.Distance(transform.position, player.transform.position) > meleeDistance)
                {
                    currentState = States.moving;
                }
                break;
            case States.dead:
                break;
        }
    }
    public override void Shoot()
    {
        Vector2 playerPos = player.transform.position;

        EnemyFire fire = Instantiate(firePrefab, bulletExit.position, firePrefab.transform.rotation);
        source.PlayOneShot(fireSound);
        fire.Setup(damage, playerPos);
        nextAttackTime = Time.time + 1f / fireRate;
        currentState = States.moving;

    }
    public override void TakeDamage(int _damage)
    {
        health -= _damage;
        if (health <= 0)
        {
            if (currentState != States.dead)
            {
                StartCoroutine(Death());
            }
        }
        else
        {
            if (canPlaySound)
            {
                StartCoroutine(SoundDelay());
            }
        }
    }
    private IEnumerator SoundDelay()
    {
        source.PlayOneShot(damagedSounds[UnityEngine.Random.Range(0, damagedSounds.Length)]);
        canPlaySound = false;
        yield return new WaitForSeconds(3);

        canPlaySound = true;
    }
    private IEnumerator Death()
    {
        Destroy(gameObject, 5);
        currentState = States.dead;
        source.PlayOneShot(deathSound);
        GetComponent<Collider2D>().enabled = false;
        GetComponent<Rigidbody2D>().isKinematic = true;
        myRb.velocity = Vector2.zero;
        yield return new WaitForSeconds(2.5f);
        Instantiate(explosion, transform.position, Quaternion.identity);
        source.PlayOneShot(explosionSound);
        onBossDefeated?.Invoke(this, EventArgs.Empty);

    }
    public void PlayIntroSound()
    {
        source.PlayOneShot(introSound);
    }
    public void GoIntoCombat()
    {
        currentState = States.moving;
    }
}
