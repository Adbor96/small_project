﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager gamemanager;

    public Transform respawnpoint;
    public GameObject playerprefab;
    public int SpawnDelay = 2;
    private EnemyRespawn enemyManager;
    private Boss boss;
    private SoundManager audioManager;

    private void Awake()
    {
        enemyManager = GameObject.Find("EnemyManager").GetComponent<EnemyRespawn>();
        gamemanager = this;
        //boss = GameObject.FindObjectOfType<Boss>();
        audioManager = GameObject.FindObjectOfType<SoundManager>();
    }

    public IEnumerator Respawn()
    {
        yield return new WaitForSeconds(SpawnDelay);

        Instantiate(playerprefab, respawnpoint.position, Quaternion.identity);
        enemyManager.Respawn();
        boss = GameObject.FindObjectOfType<Boss>();
        if(boss != null)
        {
            Debug.Log("Boss is alive");
            Destroy(boss.gameObject);
        }
        else
        {
            Debug.Log("Boss is not alive");
        }
    }

    public static void KillPlayer(Player player)
    {
        Destroy(player.gameObject);
        gamemanager.StartCoroutine(gamemanager.Respawn());
    }

}
