﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int maxHealth = 20;
    public int currentHealth;
    public HealthBar healthBar;
    private Animator anim;
    private AudioSource source;

    [SerializeField] private AudioClip[] damagedAudio;
    [SerializeField] private AudioClip deathAudio;

    private SoundManager audioManager;
    private void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        anim = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
        audioManager = GameObject.FindObjectOfType<SoundManager>();
    }

    private void Update()
    {
        if (transform.position.y <= -100)
        {
            TakeDamage(1000000);
        }
    }

    public void TakeDamage(int damage)
    {
        source.PlayOneShot(damagedAudio[Random.Range(0, damagedAudio.Length)]);
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
        anim.SetTrigger("Damaged");

        if (currentHealth <= 0)
        {
            audioManager.PlayDeathSound();
            GameManager.KillPlayer(this);
            Debug.Log("You Died!!");
        }

    }


}
