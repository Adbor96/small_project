﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour, IPooledObjects
{

    public float speed = 20f;
    public int damage = 3;
    public Rigidbody2D rb;
    private Vector3 direction;
    private Player player;

    // Start is called before the first frame update
    public void OnObjectSpawn()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        if(player.transform.position.x < transform.position.x)
        {
            rb.velocity = transform.up * speed;
        }
        else if(player.transform.position.x > transform.position.x)
        {
            rb.velocity = transform.up * speed;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy enemy = collision.gameObject.GetComponent<Enemy>();
        if (enemy != null)
        {
            //Debug.Log("Hit");
            enemy.TakeDamage(damage);
        }
        Destroy(gameObject);
    }


}
