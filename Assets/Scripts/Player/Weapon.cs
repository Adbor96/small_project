﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public Transform firePoint;
    public GameObject BulletPrefab;
    private Player player;

    private Animator anim;
    private AudioSource source;
    public AudioClip fireSound;
    public bool canShoot;

    ObjectPooler pooler;
    private void Start()
    {
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        source = GetComponent<AudioSource>();
        canShoot = true;
        pooler = ObjectPooler.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") && canShoot)
        {
            Shoot();
            anim.SetTrigger("Shoot");
        }
    }

    void Shoot()
    {
        source.PlayOneShot(fireSound);
        if(player.transform.position.x < firePoint.position.x)
        {
            pooler.SpawnFromPool("PlayerBullet", firePoint.position, BulletPrefab.transform.rotation);
            //Instantiate(BulletPrefab, firePoint.position, BulletPrefab.transform.rotation);
        }
        else if(player.transform.position.x > firePoint.position.x)
        {
            Quaternion newRotation = BulletPrefab.transform.rotation * Quaternion.Euler(0, 0, 180);
            pooler.SpawnFromPool("PlayerBullet", firePoint.position, newRotation);
            //Instantiate(BulletPrefab, firePoint.position, BulletPrefab.transform.rotation * Quaternion.Euler(0, 0, 180));
        }

    }

}
