﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private AudioClip playerDeath;
    private AudioSource source;
    private Player player;
    private bool soundPlayed;
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        player = GameObject.FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayDeathSound()
    {
        source.PlayOneShot(playerDeath);
        soundPlayed = true;
    }
}
