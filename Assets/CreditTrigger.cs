﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditTrigger : MonoBehaviour
{
    private Boss boss;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            StartCoroutine(CreditDelay());
        }
        if(boss == null)
        {
            boss = GameObject.FindObjectOfType<Boss>();
            return;
        }
        else
        {
            boss.onBossDefeated += Boss_onBossDefeated;
        }
    }

    private void Boss_onBossDefeated(object sender, System.EventArgs e)
    {
        StartCoroutine(CreditDelay());
    }
    private IEnumerator CreditDelay()
    {
        yield return new WaitForSeconds(5);
        Debug.Log("Load credits");
        SceneManager.LoadSceneAsync("Credits");
    }
}
